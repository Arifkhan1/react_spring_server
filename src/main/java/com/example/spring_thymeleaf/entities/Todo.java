package com.example.spring_thymeleaf.entities;

import com.example.spring_thymeleaf.dto.TodoResponseDTO;

import javax.persistence.*;

@Entity

public class Todo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // auto generation of unique "id" by system.
    private int id;
    @Column(nullable = false) // unique title need ,, no duplication.
    private String title;

    @Column(nullable = false)
    private String discription;

    @Column(nullable = false)
    private boolean isDone;

    @ManyToOne
    private AppUser appUser;


    public Todo(String title, String discription, AppUser appUser) {
        this.title = title;
        this.discription = discription;
        this.isDone=false;
        this.appUser=appUser;

    }

    public Todo() {
    }
 public TodoResponseDTO toResponceDTO(){

        return new TodoResponseDTO(id, appUser.getId(), title,discription,isDone);
 }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDiscription() {
        return discription;
    }

    public void setDiscription(String discription) {
        this.discription = discription;
    }

    public boolean isDone() {
        return isDone;
    }

    public void setDone(boolean done) {
        isDone = done;
    }
}
