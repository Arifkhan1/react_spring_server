package com.example.spring_thymeleaf.controller;

import com.example.spring_thymeleaf.dto.AppUserResponceDTO;
import com.example.spring_thymeleaf.dto.CreateTodoDTO;
import com.example.spring_thymeleaf.dto.TodoResponseDTO;
import com.example.spring_thymeleaf.entities.AppUser;
import com.example.spring_thymeleaf.entities.Todo;
import com.example.spring_thymeleaf.service.TodoService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/todo") // ask teacher .....request mapping  url
@CrossOrigin(origins = {"http://localhost:3000"},methods = {RequestMethod.DELETE, RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT})
public class  TodoController {
    private final TodoService todoService;

    public TodoController(TodoService todoService) {
        this.todoService = todoService;
    }

    @GetMapping  // get all toDos
    public List<TodoResponseDTO> findAll(@RequestParam(required = false, defaultValue = "", name = "tcon") String contains) {
        // @RequestParm we can make our search specific
        return todoService.findAll(contains)
                .stream()
                .map(Todo::toResponceDTO)
                .toList();
    }

    @GetMapping("/{id}") // get request via specific id
    public Todo findById(@PathVariable int id) { // must creat @Pathvariable
        return todoService.findById(id);
    }

    @PostMapping   // post data
    public TodoResponseDTO addTodo(@RequestBody CreateTodoDTO createTodoDTO) {
        return todoService
                .addTodo(createTodoDTO.title(),
                createTodoDTO.discription() ,
                        createTodoDTO.userId()).
                         toResponceDTO();

    }

    @DeleteMapping("/{id}")
    public void deletById(@PathVariable int id) {
        todoService.deletById(id);

    }
    @PutMapping("/{id}")

    public TodoResponseDTO uppDateUser(@RequestBody Todo todo, @PathVariable int id){

        return todoService.upDateTodo(todo, id).toResponceDTO();
    }


}
