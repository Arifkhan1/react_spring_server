package com.example.spring_thymeleaf.controller;

import com.example.spring_thymeleaf.dto.AppUserResponceDTO;
import com.example.spring_thymeleaf.dto.TodoResponseDTO;
import com.example.spring_thymeleaf.entities.AppUser;
import com.example.spring_thymeleaf.entities.Todo;
import com.example.spring_thymeleaf.security.AuthService;
import com.example.spring_thymeleaf.service.AppuserService;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/user")
//@Secured("ROLE_ADMIN")
@CrossOrigin(origins = {"http://localhost:3000"},methods = {RequestMethod.GET,RequestMethod.OPTIONS})

public class AppUserController {
    private final AppuserService appuserService;
    private final AuthService authService;

    public AppUserController(AppuserService appuserService, AuthService authService) {
        this.appuserService = appuserService;
        this.authService = authService;
    }
    @GetMapping
    public List<AppUserResponceDTO> findAll(){
        return appuserService.findAll()
                .stream(). peek(System.out::println)
                .map(AppUser::toRespanseTDO).toList();
    }
    @GetMapping("/{id}/todo")
    //@PreAuthorize("@authService.isMatchesUser(#userId)")
    public  List<TodoResponseDTO> findUserTodos(@PathVariable("id") int userId){
        return appuserService.findUserTodos(userId)
                .stream()
                .map(Todo::toResponceDTO).
                toList();
    }
    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable int id){
        appuserService.deletById(id);
    }


    @PutMapping("/{id}")

    public AppUserResponceDTO uppDateUser(@RequestBody AppUser appUser,@PathVariable int id){

        return appuserService.upDateUser(appUser, id).toRespanseTDO();
    }


}
