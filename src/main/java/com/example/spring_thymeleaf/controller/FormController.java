package com.example.spring_thymeleaf.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@CrossOrigin
public class FormController {

    @GetMapping("/todoform")
    public String getTodoForm(){
        return "newtodoform";
    }
}
