package com.example.spring_thymeleaf.security;

import com.example.spring_thymeleaf.entities.AppUser;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class AuthService {
    // public static boolean isNamedArif(){
    public static boolean isMatchesUser(int id) {

        AppUser appUser = (AppUser) SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getPrincipal();
        // System.out.println(appUser.getUserName());
       /* System.out.println(" the user ID"+appUser.getId());
        System.out.println( "URL id"+ id);*/
        return appUser.getId() == id;
        // return appUser.getUserName().equalsIgnoreCase("arif");

    }
}
