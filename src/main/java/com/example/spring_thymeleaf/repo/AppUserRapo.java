package com.example.spring_thymeleaf.repo;

import com.example.spring_thymeleaf.entities.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AppUserRapo extends JpaRepository<AppUser, Integer> {

 Optional<AppUser> findAppUsersByUserName(String userName);

}
