package com.example.spring_thymeleaf.dto;

public record AppUserResponceDTO(int id,
                                 String userName) {
}
