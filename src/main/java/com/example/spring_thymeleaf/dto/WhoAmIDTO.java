package com.example.spring_thymeleaf.dto;

import net.bytebuddy.implementation.bytecode.ShiftRight;

public record WhoAmIDTO(String userName, String id, String token) {
}
