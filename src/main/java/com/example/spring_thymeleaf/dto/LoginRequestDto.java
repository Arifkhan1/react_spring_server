package com.example.spring_thymeleaf.dto;

public record LoginRequestDto(String userName, String password) {
}
