package com.example.spring_thymeleaf.dto;

public record CreateTodoDTO(String title,
                            String discription,
                            int userId) {


}
