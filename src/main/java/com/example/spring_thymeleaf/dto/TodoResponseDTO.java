package com.example.spring_thymeleaf.dto;

public record TodoResponseDTO(int id,
                              int appUserId,
                              String title ,
                              String discription,
                              boolean Isdone) {
}
