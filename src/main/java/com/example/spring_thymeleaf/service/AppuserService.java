package com.example.spring_thymeleaf.service;

import com.example.spring_thymeleaf.dto.TodoResponseDTO;
import com.example.spring_thymeleaf.entities.AppUser;
import com.example.spring_thymeleaf.entities.Todo;
import com.example.spring_thymeleaf.repo.AppUserRapo;
import com.example.spring_thymeleaf.repo.TodoRepo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AppuserService {

    private final AppUserRapo appUserRapo; // dependency injection
    private final TodoRepo todoRepo;


    public AppuserService(AppUserRapo appUserRapo, TodoRepo todoRepo) {
        this.appUserRapo = appUserRapo;
        this.todoRepo = todoRepo;
    }

    public List<AppUser> findAll() {
       return appUserRapo.findAll();


    }

    public List<Todo> findUserTodos(int userId) {
       return todoRepo.findTodosByAppUser_Id(userId);
}

    public void deletById(int id) {
        appUserRapo.deleteById(id);
    }

    public AppUser upDateUser(AppUser appUser,int id) {
        Integer user_Id = appUser.getId();
        AppUser userUpdate = appUserRapo.findById(id).orElseThrow();// find the spacific user to be updated
        userUpdate.setUserName(appUser.getUserName());
        userUpdate.setPassword(appUser.getPassword());
        userUpdate.setRoles(appUser.getRoles());
        return appUserRapo.save(userUpdate);

    }
}
