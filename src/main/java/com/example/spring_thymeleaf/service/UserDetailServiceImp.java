package com.example.spring_thymeleaf.service;

import com.example.spring_thymeleaf.repo.AppUserRapo;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailServiceImp implements UserDetailsService {

    private final AppUserRapo appUserRapo;

    public UserDetailServiceImp(AppUserRapo appUserRapo) {
        this.appUserRapo = appUserRapo;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return appUserRapo.findAppUsersByUserName(username).orElseThrow();
    }
}
