package com.example.spring_thymeleaf.service;

import com.example.spring_thymeleaf.entities.AppUser;
import com.example.spring_thymeleaf.entities.Todo;
import com.example.spring_thymeleaf.repo.AppUserRapo;
import com.example.spring_thymeleaf.repo.TodoRepo;
import org.springframework.stereotype.Service;

import java.util.List;
@Service

public class TodoService {
    private final TodoRepo todoRepo;
    private final AppUserRapo appUserRapo;

    public TodoService(TodoRepo todoRepo, AppUserRapo appUserRapo) {
        this.todoRepo = todoRepo;
        this.appUserRapo = appUserRapo;
    }

    public List<Todo> findAll(String contains) {
        if(contains.isBlank()){
        return todoRepo.findAll();

    }
        return todoRepo.findTodosByTitleContainsIgnoreCase(contains);
    }

    public Todo findById(int id) {
        return todoRepo.findById(id).orElseThrow();
    }

    public Todo addTodo(String title, String discription, int userId) {
        AppUser appUser = appUserRapo.findById(userId).orElseThrow();
        return todoRepo.save(new Todo(title,discription,appUser));
    }

    public void deletById(int id) {
        todoRepo.deleteById(id);
    }

    public Todo upDateTodo(Todo todo, int id) {

       /* Integer user_Id = appUser.getId();
        AppUser userUpdate = appUserRapo.findById(id).orElseThrow();// find the spacific user to be updated
        userUpdate.setUserName(appUser.getUserName());
        userUpdate.setPassword(appUser.getPassword());
        userUpdate.setRoles(appUser.getRoles());
        return appUserRapo.save(userUpdate);*/

      //  Integer User_Id = todo.getId();

        Todo todoUpdate = todoRepo.findById(id).orElseThrow();
        todoUpdate.setTitle(todo.getTitle());
        todoUpdate.setDiscription(todo.getDiscription());
        return todoRepo.save(todoUpdate);
    }
}
