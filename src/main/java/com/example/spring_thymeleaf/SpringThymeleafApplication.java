package com.example.spring_thymeleaf;

import com.example.spring_thymeleaf.entities.AppUser;
import com.example.spring_thymeleaf.entities.Role;
import com.example.spring_thymeleaf.entities.Todo;
import com.example.spring_thymeleaf.repo.AppUserRapo;
import com.example.spring_thymeleaf.repo.TodoRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;
import java.util.Set;

@SpringBootApplication

public class SpringThymeleafApplication implements CommandLineRunner {
    @Autowired
    TodoRepo todoRepo;
    @Autowired
    AppUserRapo appUserRapo;
    @Autowired
    PasswordEncoder passwordEncoder;

    public static void main(String[] args) {
        SpringApplication.run(SpringThymeleafApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
       AppUser appUser2 =  appUserRapo.save(new AppUser("irfan",passwordEncoder.encode("123") , Set.of(Role.ADMIN) ));
        todoRepo.saveAll(List.of(new Todo("Irfan's 1st todos","Apple Aple APPLe",appUser2),
                new Todo ("Irfan,s 2nd ","bus bus bus bus ",appUser2)

        ));

        AppUser appUser = appUserRapo.save(new AppUser("arif","123" , Set.of(Role.USER)));
        todoRepo.saveAll(List.of(new Todo("Arif first Todo","This is a brif description of todo",appUser),
                                new Todo ("Arif second Todo","This is a brif description of  second todo",appUser)

                ));
    }
}
